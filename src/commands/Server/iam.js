const GuildSARs = require('../../dbFunctions/message/sar');

module.exports.run = async (client, message, role) => {
  const sar = new GuildSARs(message.guild.id);

  if (role.length === 0) return message.send('❌ `|` 📋 **You didn\'t give me a role to add!**');
  role = role.join(' ');

  try { await message.functions.parseRole(role); } catch (e) { return message.send(`❌ \`|\` 📋 **${e}**`); }
  const sarRole = await message.functions.parseRole(role);
  
  const sarRoles = await sar.sarRoles;
  if(!sarRoles.includes(sarRole.id)) return message.send('❌ `|` 📋 **This role is not self-assignable!**');

  message.member.roles.add(sarRole);
  message.send(`✅ \`|\` 📋 **Given role** \`${sarRole.name}\``);
};

exports.conf = {
  enabled: true,
  aliases: ['assignselfassignablerole'],
  guildOnly: true,
  permLevel: 'User'
};

exports.help = {
  name: 'iam',
  description: 'Give yourself a self-assignable role',
  usage: 'iam <role/role ID>',
  category: 'Server'
};