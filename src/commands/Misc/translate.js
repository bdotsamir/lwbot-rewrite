const { MessageEmbed } = require('discord.js');
const iso = require('iso-639-1');
const fetch = require('node-fetch');

module.exports.run = (client, message, args) => {
  if (!args[0]) return message.send('❌ `|` **You didn\'t give me any message to translate!**');

  fetch(`https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=en&dt=t&q=${encodeURIComponent(args.join(' '))}`)
    .then(res => res.json())
    .then(res => {
      let combined = '';

      for(const translatedString of res[0]) {
        combined += translatedString[0];
      }

      const embed = new MessageEmbed()
        .setAuthor('Translated Message')
        .setDescription(combined)
        .setColor(client.accentColor)
        .setFooter(`Detected language: ${iso.getName(res[2])}`);

      message.send(embed);
    }).catch(e => {
      return message.send('❌ `|` Failed to get data. ' + e);
    });
};

exports.conf = {
  enabled: true,
  aliases: [],
  permLevel: 'User',
  guildOnly: false,
  cooldown: 10000 // 10 seconds
};

exports.help = {
  name: 'translate',
  description: 'Translate a string',
  usage: 'translate <string>',
  category: 'Misc'
};