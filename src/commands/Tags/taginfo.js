const Tags = require('../../dbFunctions/client/tags').tagsTable;
const moment = require('moment');

module.exports.run = async (client, message, args) => {
  const tagName = args;

  // equivalent to: SELECT * FROM tags WHERE name = 'tagName' LIMIT 1;
  const tag = await Tags.findOne({ where: { name: tagName } });
  if (tag) return message.send(`ℹ \`|\` :pencil: **\`${tagName}\` created by \`${tag.username}\` at \`${moment(tag.createdAt).format('MM/DD/YYYY HH:mm')}\`\n\t\t Used ${tag.usage_count} times**`);
  else return message.send(`❌ \`|\` :pencil: **\`${tagName}\` does not exist**`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 'User'
};

exports.help = {
  name: 'taginfo',
  description: 'Shows the information of particular tag',
  usage: 'taginfo <tag name>',
  category: 'Tags'
};