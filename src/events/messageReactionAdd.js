// I'm using this event solely for the starboard functionality.
const { MessageEmbed } = require('discord.js');
const starMap = new Map();

module.exports = async (client, reaction, user) => {
  if(global.failover || !client.ready) return; // Disable this module in failover mode

  // If the reaction is not a star, return
  if(reaction.emoji.name !== '⭐') return;

  // Get the guild settings
  const settings = client.settings.get(reaction.message.guild.id);

  const starboardEnabled = settings['starboardEnabled'] === 'true' ? true : false;
  const maxStars = settings['starsNeeded'];
  const starboardChannel = settings['starboardChannel'];

  if(!starboardEnabled) return;

  // If the user who reacted is also the author of the message, remove the reaction ;)
  if(reaction.message.author.id === user.id) return reaction.message.reactions.cache.get('⭐').users.remove(user);

  // If the starred user is a bot, return.
  if(reaction.message.author.bot) return;

  const numStars = reaction.message.reactions.cache.get('⭐').count;

  // This message already has stars on it. Update the starboard embed with the new count.
  if(starMap.has(reaction.message.id)) {
    const message = starMap.get(reaction.message.id); // Returns a message object
    message.embeds[0].setFooter(`${formatStars(numStars)} ${numStars} stars`);

    return message.edit(message.embeds[0]);
  }

  // If the number of stars on the message is ≥ the maximum amount of stars needed, post the message to the starboard
  if(numStars >= maxStars) {
    const starEmbed = new MessageEmbed()
      .setColor('0xE8B923') // gold color
      .setAuthor(reaction.message.member.displayName, reaction.message.author.displayAvatarURL())
      .setDescription(reaction.message.content)
      .addField('Jump to message', reaction.message.url)
      .setFooter(`${formatStars(numStars)} ${numStars} stars`)
      .setTimestamp();

    if(reaction.message.attachments.size > 0) starEmbed.setImage(reaction.message.attachments.first().url);

    const sendTo = reaction.message.guild.channels.cache.find(g => g.name === starboardChannel);
    if(!sendTo) return;

    return sendTo.send(starEmbed)
      .then(msg => {
        // Store this message in the set of messages that have star data embeds so we can update them later if they get more stars
        starMap.set(reaction.message.id, msg);
      });
  }
};

function formatStars(count) {
  if(count > 30) return '🎆'; // wow, that's a lot of stars :woag:
  else if(count > 20) return '🌠';
  else if(count > 10) return '🌟';
  else return '⭐';
}