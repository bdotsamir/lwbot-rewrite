//const { MessageEmbed } = require('discord.js');
const { readdirSync, writeFile, access, mkdir, constants } = require('fs');
const { readFile } = require('fs').promises;
const { join } = require('path');
const child_process = require('child_process');
const { unlink } = require('fs');

const brains = readdirSync('./brains/');
const brainsWithoutJSON = brains.map(g => g.split('.json')[0]);
const brainsDir = join(__dirname, '../../brains');

module.exports.run = async (client, message, [...IDs]) => {

  // Temporary directory for brains when they're being trained
  access(`./tmp/brain-${message.guild.id}`, constants.F_OK, err => {
    if (err) return false;
    else return message.send('❌ `|` 🧠 **This guild\'s network is already being trained!** ');
  });

  if (IDs.length === 0) return message.send('❌ `|` 🧠 **You didn\'t give an ID / IDs of messages to add to the network!**');

  // Sort through and make sure all the messages provided actually exist
  const msgsThatDoExist = [];
  for (const id of IDs) {
    const msg = await message.channel.messages.fetch(id)
      .catch(e => { if (e.message === 'Unknown Message') return; else throw e; });

    if (msg) msgsThatDoExist.push(msg);
  } // Yes, I tried .map, unfortunately it didnt work for whatever reason.

  // If none of the messages exist, error
  if (msgsThatDoExist.length === 0) return message.send(`❌ \`|\` 🧠 **${IDs.length === 1 ? 'The message provided does not exist!' : 'One of the messages provided exist!'}** `);

  // If there's already a JSON file for the guild, load it in first
  const brainData = JSON.parse((await readFile(`${brainsDir}/${message.guild.id}.json`)).toString());
  if (brainsWithoutJSON.includes(message.guild.id)) message.guild.brain.fromJSON(brainData);

  const trainingData = [];
  for (const msg of msgsThatDoExist) trainingData.push({ input: msg.content, output: '1' });

  const msg = await message.send('<a:loading:536942274643361794> `|` 🧠 **Training...**');

  access('./tmp/', accessErr => {
    if (accessErr && accessErr.code === 'ENOENT') {
      mkdir('./tmp/', mkdirError => {
        if (mkdirError) return msg.edit(`❌ \`|\` 🧠 **Error creating tmp directory:**\n\`${mkdirError}\``);
        client.logger.verbose('trainnetwork.js: Created temp directory at src/tmp');
      });
    }

    writeFile(`./tmp/brain-${message.guild.id}`, ' ', writeFileErr => {
      if (writeFileErr) return msg.edit(`❌ \`|\` 🧠 **Error writing to tmp file:**\n\`${writeFileErr}\``);
      client.logger.verbose(`trainnetwork.js: Wrote temp file: brain-${message.guild.id}.json`);
    });
  });

  trainBrain()
    .then(result => {
      console.log('Brain training result:', result);
    })
    .catch(e => {
      client.logger.error(e);
      msg.edit(`❌ \`|\` 🧠 **Training error:** \`${e}\``);
    });

  function trainBrain() {
    return new Promise((resolve, reject) => {
      const dataToSend = {
        client, // Send the whole client object,
        trainingData, // Send the training data
        guildID: message.guild.id, // The corresponding guild ID
        brain: message.guild.brain.toJSON() // And the brain to train, in JSON format
      };

      const forkOptions = {
        serialization: 'json' // If I dont set this it errors. Check the node ChildProcess docs for more info
      };
      /* explaination:           .fork(script path,            args, fork options) */
      const child = child_process.fork('./util/trainbrain.js', null, forkOptions); // Create the child process
      child.send(dataToSend); // Send process information to the child process

      child.on('message', async childMessage => {
        if (typeof childMessage !== 'object') reject(new TypeError('childMessage was not an object.'));

        // The network is making progress! Update the initial message accordingly
        if (childMessage.type === 'progress') {
          const iterations = +childMessage.message.match(/\d+/g)[0]; // It's a string, but the + at the beginning of this expression turns it into a number.
          const percentage = `${Math.round(iterations / 20000 * 100)}%`;

          await msg.edit(`<a:loading:536942274643361794> \`|\` 🧠 **Training...** \`${percentage}\``);
        }
        // The network is finished! Save all the data
        else if (childMessage.type === 'finished') {

          // Load the now-trained brain into the guild object
          message.guild.brain.fromJSON(childMessage.brain);
          // Save the brain to disk
          writeFile(`${brainsDir}/${message.guild.id}.json`, JSON.stringify(message.guild.brain.toJSON()), err => {
            if (err) return reject(err);
            client.logger.verbose(`Wrote new brain data for ${message.guild.id}`);
          });

          // Delete the temp file
          unlink(`./tmp/brain-${message.guild.id}`, err => {
            if (err) return reject(err);
            client.logger.verbose(`Deleted the tmp brain file for ${message.guild.id}`);
          });

          resolve(childMessage.message);
        } else {
          reject(new Error(`Child process sent invalid message: \n${JSON.stringify(childMessage, null, 2)}`));
        }
      });

      child.on('error', reject); // Obviously, if there was an error, reject.
      child.on('exit', reject); // This rejects because the process isn't supposed to exit before a message is sent.
    });
  }

  /* 
  * TODO: https://github.com/BrainJS/brain.js/pull/382 
  * - PR creates .trainAsync on an LSTM NN.
  * Once this PR is accepted I won't have to worry about child processes anymore. It'll be async
  */
  // This is trainAsync because regular train locks the thread.
  //message.guild.brain.trainAsync(trainingData)
  //  .then(res => message.send(res))
  //  .catch(e => message.send(`❌ \`|\` 🧠 **There was an error training the brain:** \`${e}\``));
};

exports.conf = {
  enabled: true,
  aliases: ['tn'],
  permLevel: 'Moderator',
  guildOnly: true,
  requiresEmbed: true,
  //disabledReason: 'See this issue: https://github.com/BrainJS/brain.js/issues/532'
};

exports.help = {
  name: 'trainnetwork',
  description: 'Add a message/messages to the neural network that detects bad messages - Message IDs must be in the same channel as the command',
  usage: 'trainnetwork <message ID / IDs>',
  category: 'Server'
};