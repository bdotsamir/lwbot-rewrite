const { version } = require('../../package.json');

module.exports = async (client, guild) => {
  client.logger.log(`Joined guild ${guild.name} (${guild.id})`);

  require('../util/sqWatchdog').runner(client, guild.id); // Runs sync-up for the guild. Settings, commands, xp

  if (!guild.me.permissions.has('SEND_MESSAGES')) guild.owner.send('❌ **CRITICAL PERMISSION MISSING:** `Send Messages` **WHICH EVERYTHING REQUIRES!**');
  if (!guild.me.permissions.has('EMBED_LINKS')) guild.owner.send('❌ **CRITICAL PERMISSION MISSING:** `Embed Links` **WHICH EVERYTHING REQUIRES!**');

  let textErrored = false;
  let voiceErrored = false;
  let mutedRoleCreateError = false;

  let role = await guild.roles.cache.find(g => g.name.toLowerCase() === 'muted');
  if (!role) {
    role = await guild.roles.create({ data: { name: 'Muted', color: 'DARK_ORANGE', position: guild.me.roles.highest.position - 1 }, reason: 'Guild Create | Guild setup!' })
      .then(role => { client.logger.verbose(`guildCreate | Created muted role in ${guild.name} (${guild.id})`); owPerms(role); })
      .catch(e => { mutedRoleCreateError = true; client.logger.verbose(e); });
  }
  else { owPerms(role); }

  async function owPerms(role) {
    await guild.channels.cache.filter(g => g.type === 'text' || g.type === 'news').forEach(channel => {
      if (guild.me.permissionsIn(channel).serialize()['MANAGE_ROLES']) {
        channel.createOverwrite(role, { SEND_MESSAGES: false, ADD_REACTIONS: false }, 'Initial Setup Process')
          .then(() => client.logger.verbose(`guildCreate | Wrote permissions for text channel #${channel.name} (${channel.id}) in ${guild.name} (${guild.id})`))
          .catch(e => { client.logger.verbose(e); textErrored = true; });
      }
      else client.logger.verbose(`guildCreate | Tried to write permissions to text channel #${channel.name} (${channel.id}) in ${guild.name} (${guild.id}), but I'm missing the MANAGE_ROLES permission.`);
    });
    await guild.channels.cache.filter(g => g.type === 'voice').forEach(channel => {
      if (guild.me.permissionsIn(channel).serialize()['MANAGE_ROLES']) {
        channel.createOverwrite(role, { CONNECT: false, SPEAK: false }, 'Inital Setup Process')
          .then(() => client.logger.verbose(`guildCreate | Wrote permissions for voice channel ${channel.name} (${channel.id}) in ${guild.name} (${guild.id})`))
          .catch(e => { client.logger.verbose(e); voiceErrored = true; });
      }
      else client.logger.verbose(`guildCreate | Tried to write permissions to voice channel ${channel.name} (${channel.id}) in ${guild.name} (${guild.id}), but I'm missing the MANAGE_ROLES permission.`);
    });
  }

  if (mutedRoleCreateError) {
    let msg = '❌ **Something went wrong during my setup process.**\n`-` I was unable to create a Muted role.';
    if (textErrored) msg += '\n`-` I was unable to disallow the Muted role from talking in text channels.';
    if (voiceErrored) msg += '\n`-` I was unable to disallow the Muted role from talking in voice channels.';
    msg += '\n\n**Please re-invite me.** When you invite me, please make sure I have all the permissions indicated on the invite page.';
    guild.owner.send(msg);
    guild.leave();
    client.logger.verbose('Encountered some errors while assigning permissions. Sent the owner a message about it; I\'m leaving now.');
  }

  // When the bot joins the server, check to see if they have the mod announcement channel
  if (guild.publicUpdatesChannel) {
    client.logger.verbose('guildCreate | Guild has publicUpdatesChannel!');

    // If the bot doesn't have permission to send to the mod updates channel, update perms so it can.
    if (!guild.publicUpdatesChannel.permissionsFor(client.user).has('SEND_MESSAGES')) {
      guild.publicUpdatesChannel.createOverwrite(client.user.id, {
        SEND_MESSAGES: true
      }, 'This is necessary so I can relay important information to you!');
    }

    const messageToSend =
      ':wave: **Hello! Thank you for inviting me!** :smiley:\n\n' +

      `I'm **${client.user.username}**, a bot that's almost general purpose. I say "almost" because, although I'm on version \`${version}\`, I'm still kind of in beta.\n` +
      'If you notice any problems with me, please feel free to report them to the LWBot Support server here: <https://discord.gg/225Kyt5>. I am by no means a perfect bot.. yet ;)\n\n' +

      ':information_source: **You\'ll want to start your journey by sending `!w help` to find out what commands I offer.**\n' +
      ':gear: **After you\'ve done that, send `!w set` to find out how you can configure me to suit your server\'s needs.**\n\n' +

      'As the dev needs to communicate with you, he\'ll send a message through me to this channel. These messages could be new feature announcements, bug fixes, or other major changes. I promise he won\'t spam you :)\n' +
      '*I figured out that this is the channel to send to because your server is **community-enabled** and this is the **Community Updates Channel***\n\n' +

      '**If you have any questions or suggestions, please don\'t hesitate to stop by the support server and drop the dev a line!**\n\n' +

      `*Sincerely,*\n**${client.user.username}**`;

    guild.publicUpdatesChannel.send(messageToSend)
      .then(() => {
        client.logger.verbose(`guildCreate | Sent Hello! message to guild to publicUpdatesChannel #${guild.publicUpdatesChannel.name} (${guild.publicUpdatesChannelID}) in ${guild.name} (${guild.id})`);
      })
      .catch(e => {
        client.logger.verbose(`guildCreate | Failed to send Hello! message to publicUpdatesChannel in ${guild.name} (${guild.id})`);
        client.logger.verbose(e);
      });
  }

  if (client.config.ciMode) client.emit('ciStepChannelCreate');
};
