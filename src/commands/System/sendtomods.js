module.exports.run = (client, message, args) => {
  if (!args[0]) return message.send('❌ `|` 📤 **Missing a message to send!**');

  const msg = args.join(' ');

  client.guilds.cache.forEach(guild => {
    if (!guild.publicUpdatesChannel) return; // Guild does not have the mod update channel enabled. Return.

    guild.publicUpdatesChannel.send(msg)
      .catch(e => {
        client.logger.verbose(`Tried to broadcast message to ${guild.id} but failed:`);
        client.logger.verbose(e);
      });
  });

  message.send(`✅ \`|\` 📤 **Sent to \`${client.guilds.cache.filter(g => g.publicUpdatesChannel).size} / ${client.guilds.cache.size}\` guilds.**`);
};

exports.conf = {
  enabled: true,
  aliases: ['broadcast'],
  guildOnly: false,
  permLevel: 'Bot Admin'
};

exports.help = {
  name: 'sendtomods',
  description: 'Send a message to every guild\'s publicUpdatesChannel (if it\'s enabled)',
  usage: 'sendtomods <message>',
  category: 'System'
};
