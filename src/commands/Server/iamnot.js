const GuildSARs = require('../../dbFunctions/message/sar');

module.exports.run = async (client, message, role) => {
  const sar = new GuildSARs(message.guild.id);

  if (role.length === 0) return message.send('❌ `|` 📋 **You didn\'t give me a role to remove!**');
  role = role.join(' ');

  try { await message.functions.parseRole(role); } catch (e) { return message.send(`❌ \`|\` 📋 **${e}**`); }
  const sarRole = await message.functions.parseRole(role);
  
  const sarRoles = await sar.sarRoles;
  if(!sarRoles.includes(sarRole.id)) return message.send('❌ `|` 📋 **This role is not self-assignable!**');

  message.member.roles.remove(sarRole);
  message.send(`✅ \`|\` 📋 **Removed role** \`${sarRole.name}\``);
};

exports.conf = {
  enabled: true,
  aliases: ['takeselfassignablerole'],
  guildOnly: true,
  permLevel: 'User'
};

exports.help = {
  name: 'iamnot',
  description: 'Remove a self-assignable role from yourself',
  usage: 'iamnot <role/role ID>',
  category: 'Server'
};