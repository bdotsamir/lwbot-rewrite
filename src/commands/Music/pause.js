module.exports.run = (client, message) => {
  const voiceChannel = message.member.voice.channel;
  if(!voiceChannel) return message.send('❌ `|` 🎵 **You aren\'t in a voice channel!**');

  const music = client.musicQueue.get(message.guild.id);
  if(!music) return message.send('❌ `|` 🎵 **There\'s no music playing!');

  // This is quite a rare condition where the user tries to pause a song when
  // the bot isn't playing anything but has an active queue. This prevents an error from
  // popping up in chat.
  if(!music.connection.dispatcher) return message.send('⚠️ `|` 🎵 **I\'m in the middle of loading a new song!** Please wait a few moments!');

  if(music.connection.dispatcher.paused) return message.send('❌ `|` 🎵 **Already paused!**');
  music.connection.dispatcher.pause();
  clearInterval(music.playing.interval);
  message.send('⏸ `|` 🎵 **Paused.**');

  music.pauseTimeout = setTimeout(async () => {
    music.songs = await [];
    music.playing.duration = await 0;
    await clearInterval(music.playing.interval);
    message.send('⏱ `|` 🎵 **I left because I was paused for more than 5 minutes.**');
    music.connection.dispatcher.end();
    music.connection.channel.leave();
  }, 300000); // 5 minutes
};

exports.conf = {
  enabled: false,
  aliases: ['⏸'],
  permLevel: 'DJ',
  guildOnly: true,
  failoverDisabled: true
};

exports.help = {
  name: 'pause',
  description: 'Pause the current song',
  usage: 'pause',
  category: 'Music'
};