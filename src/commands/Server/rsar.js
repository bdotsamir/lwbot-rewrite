const GuildSARs = require('../../dbFunctions/message/sar');

module.exports.run = async (client, message, role) => {
  const sar = new GuildSARs(message.guild.id);

  if (role.length === 0) return message.send('❌ `|` 📋 **You didn\'t give me a role to remove!**');
  role = role.join(' ');

  try { await message.functions.parseRole(role); } catch (e) { return message.send(`❌ \`|\` 📋 **${e}**`); }
  const sarRole = await message.functions.parseRole(role);

  await sar.deleteRole(sarRole.id);

  message.send(`✅ \`|\` 📋 **Removed role** \`${sarRole.name}\``);
};

exports.conf = {
  enabled: true,
  aliases: ['removeselfassignablerole', 'delsar'],
  guildOnly: true,
  permLevel: 'User'
};

exports.help = {
  name: 'rsar',
  description: 'Remove a self-assignable role from this server',
  usage: 'rsar <role/role ID>',
  category: 'Server'
};