const Tags = require('../../dbFunctions/client/tags').tagsTable;

module.exports.run = async (client, message, args) => {
  const tagName = args;
  // equivalent to: SELECT * FROM tags WHERE name = 'tagName' LIMIT 1;
  const tag = await Tags.findOne({ where: { name: tagName } });
  if (tag) {
    // equivalent to: UPDATE tags SET usage_count = usage_count + 1 WHERE name = 'tagName';
    tag.increment('usage_count');
    return message.send(tag.get('description'));
  }
  return message.send(`❌ \`|\` :pencil: **\`${tagName}\` does not exist**`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['findtag'],
  permLevel: 'User'
};

exports.help = {
  name: 'tag',
  description: 'Find a tag particular tag',
  usage: 'tag <tag name>',
  category: 'Tags'
};