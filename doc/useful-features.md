# Useful Features

## Command editing
Something I don't see very often in bots is the ability to edit your initial command and have the correct corresponding output.. I know that's not quite descriptive so let me show you a gif to help you understand:

![](assets/command-editing.gif)

## Music
This bot has music capabilities! Simply do `!w play <name of video / youtube link>` and the bot will start playing it for you. If a song is currently playing, it will add the song to the queue, which can be viewed by typing `!w queue`.
> **Please note that because this is a resource-intensive process, playing music costs 1000 Kowoks.** Kowoks are the bot's currency, which currently can only be acquired by doing `!w daily`, which will give you 200 Kowoks.

<small style="font-size: 10px;">*Technically there is a way to get around this.. just get multiple people to run the daily command and send it to whoever is queuing the music. That's what I would do if I found out music required funds ¯\\\_(ツ)_/¯*</small>

As I add more features that aren't present on most other bots I'll update this list. But for now this is all I got :sob: