# Bot Documentation
Because this thing is confusing sometimes.

(Note that every time `!w ` is used, that's the default prefix. Just substitute it for whatever prefix you've configured.)

## Table of contents:
* [Settings documentation](./settings.md)
* [Useful features](./useful-features.md)