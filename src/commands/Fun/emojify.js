const cursedEmojis = ['😳', '🙈', '🥴', '👀', '🥵', '👈', '😡', '📲', '😂', '😏', '😤', '😜', '💯', '🤬', '🅱️', '🥺', '😋', '😈', '😍'];

module.exports.run = (client, message, args) => {
  if (!args[0]) return message.send('❌ `|` **😳 You 👈 didn\'t 😡 give 🎁 me 🤙 any 👀 text 📲 to 🥴 emojify! 😂**');

  let msg = '';
  for (const arg of args) {
    msg += `${arg} ${cursedEmojis.randomElement()} `;
    // I do it like this because the alternative,
    //  message.send(args.join(` ${cursedEmojis.randomElement()} `))
    // chose the same emoji every time
  }

  if (msg.length > 2000) msg = msg.substring(0, 2000); // Truncate the message

  message.send(msg);
};

exports.conf = {
  enabled: true,
  aliases: [],
  guildOnly: false,
  permLevel: 'User',
  hidden: true
};

exports.help = {
  name: 'emojify',
  description: 'Add 😳 emojis 🙈 between 🥴 some 👀 text 🥵',
  usage: 'emojify <text>',
  category: 'Fun'
};