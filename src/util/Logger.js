// Logger class for easy and aesthetically pleasing console logging

const chalk = require('chalk');
const moment = require('moment');
const config = require('../config');
const { mkdir, appendFile } = require('fs-extra');

exports.log = (content, type = 'log') => {
  const timestamp = `${chalk.grey(`[${moment().format('MM/DD/YYYY HH:mm:ss')}]`)}${global.failover ? chalk.yellow(' F') : ''}`;
  switch (type) {
    case 'log': {
      appendToLog('info', content, true, true);
      return console.log(`${timestamp} ${chalk.blue('info:')} ${content} `);
    }
    case 'warn': {
      appendToLog('warn', content, true, true);
      return console.warn(`${timestamp} ${chalk.yellow('warn:')} ${content} `);
    }
    case 'error': {
      if (content.stack || (typeof content === 'string' && content.includes('\n'))) {
        content = content.stack || content;
        return console.warn(`${timestamp} ${chalk.red('error:')} ${content.split('\n').join(`\n${timestamp} ${chalk.red('error: ')}`)} `);
      } else console.warn(`${timestamp} ${chalk.red('error:')} ${content} `);
      return appendToLog('error', content, true, true);
    }
    case 'fatal': {
      if (content.stack || (typeof content === 'string' && content.includes('\n'))) {
        content = content.stack || content;
        return console.error(`${timestamp} ${chalk.bgRed.white('FATAL:')} ${chalk.red(content.split('\n').join(`\n${timestamp} ${chalk.bgRed.white('FATAL:')}`))} `);
      } else console.error(`${timestamp} ${chalk.bgRed.white('FATAL:')} ${chalk.red(content)} `);
      return appendToLog('fatal', content, true, true);
    }
    case 'debug': {
      appendToLog('debug', content, false, true);
      if (config.debugMode) return console.log(`${timestamp} ${chalk.cyan('debug:')} ${chalk.gray(content)} `);
      break;
    }
    case 'verbose': {
      if (typeof content === 'object') content = require('util').inspect(content, { depth: 0, colors: true });
      if (typeof content === 'string' && (content.stack || content.includes('\n'))) content = content.stack || content.split('\n').join(`\n${timestamp} ${chalk.gray('verbose: ')}`);
      appendToLog('verbose', content, false, true);
      if (config.verboseMode) return console.log(`${timestamp} ${chalk.gray('verbose:')} ${chalk.gray(content)} `);
      break;
    }
    case 'sqLog': {
      appendToLog('sqLog', content, false, true);
      if (config.sqLogMode) return console.log(`${timestamp} ${chalk.white.bgBlack('sqlog:')} ${chalk.gray(content)} `);
      break;
    }
    case 'cmd': {
      appendToLog('cmd', content, true, true);
      return console.log(`${timestamp} ${chalk.blue('cmd:')} ${content}`);
    }
    case 'ws': {
      appendToLog('ws', content, true, true);
      return console.log(`${timestamp} ${chalk.blue('ws:')} ${content}`);
    }
    case 'ready': {
      appendToLog('info', content, true, true);
      return console.log(`${timestamp} ${chalk.green('ready:')} ${chalk.green(content)}`);
    }
    case 'reconnecting': {
      appendToLog('info', content, true, true);
      return console.log(`${timestamp} ${chalk.yellow('reconnecting:')} ${content}`);
    }
    case 'disconnect': {
      appendToLog('info', content, true, true);
      return console.warn(`${timestamp} ${chalk.red('disconnect:')} ${content} `);
    }
    case 'resume': {
      appendToLog('info', content, true, true);
      return console.log(`${timestamp} ${chalk.green('resume:')} ${content}`);
    }
    default: throw new TypeError('Logger type must be either warn, debug, log, ready, cmd or error.');
  }
};

exports.error = (...args) => this.log(...args, 'error');
exports.fatal = (...args) => this.log(...args, 'fatal');
exports.warn = (...args) => this.log(...args, 'warn');
exports.debug = (...args) => this.log(...args, 'debug');
exports.cmd = (...args) => this.log(...args, 'cmd');
exports.debug = (...args) => this.log(...args, 'debug');
exports.verbose = (...args) => this.log(...args, 'verbose');
exports.sqLog = (...args) => this.log(...args, 'sqLog');
exports.ws = (...args) => this.log(...args, 'ws');
exports.websocket = (...args) => this.log(...args, 'ws');

async function appendToLog(type, content, combined = true, combinedDebug = true) {
  if (config.noFileLog) return;

  const timestamp = moment().format('MM/DD/YYYY HH:mm:ss');
  const currentDayLog = moment().format('YYYY-MM-DD');

  await mkdir(`./logs/${currentDayLog}`, { recursive: true }, e); // Makes ../logs/ if it not already exist
  await appendFile(`./logs/${currentDayLog}/${type}.log`, `${timestamp} ${content}\n`, e, type);

  if (combined) await appendFile(`./logs/${currentDayLog}/combined.log`, `${timestamp} ${type.toUpperCase()} ${content}\n`, e);
  if (combinedDebug) await appendFile(`./logs/${currentDayLog}/combinedDebug.log`, `${timestamp} ${type.toUpperCase()} ${content}\n`, e);
}
exports.appendToLog = appendToLog; // Export it for other files' usage

function e(e, type) {
  if (e && e.code === 'EEXIST') return;
  if (e && e.code === 'ENOENT') {
    const curDay = moment().format('YYYY-MM-DD');
    mkdir(`./logs/${curDay}${type ? `/${type}.log` : ''}`, { recursive: true })
      .then(() => this.log(`Created log directory for today: ${curDay}`))
      .catch(e => {
        if (e.code === 'EEXIST') return;
        else this.log(e);
      });
  }
  if (e) console.error('Appendfile error: ' + e);
}